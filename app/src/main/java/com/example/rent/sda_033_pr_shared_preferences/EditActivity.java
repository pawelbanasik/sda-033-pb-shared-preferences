package com.example.rent.sda_033_pr_shared_preferences;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class EditActivity extends AppCompatActivity {

    // nazwa pliku gdzie bedzie nasza aplikacja
    public static final String PREFERENCES_PATH = "prefs.db";
    public static final String PREFERENCE_NAME = "user_name";
    public static final String PREFERENCE_SURNAME = "user_surname";
    public static final String PREFERENCE_LANG = "user_lang";
    public static final String PREFERENCE_COUNTRY = "user_country";


    @BindView(R.id.edit_name)
    protected EditText editName;

    @BindView(R.id.edit_surname)
    protected EditText editSurname;

    @BindView(R.id.spinner_country)
    protected Spinner spinnerCountry;

    @BindView(R.id.spinner_language)
    protected Spinner spinnerLanguage;

    @BindView(R.id.button_save)
    protected Button buttonSave;

    @BindView(R.id.button_cancel)
    protected Button buttonCancel;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);
        ButterKnife.bind(this);

        // ustawiam zawartosc spinnerow ze strings
        ArrayAdapter<String> adapterCountries = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_dropdown_item,
                getResources().getStringArray(R.array.countries));
        spinnerCountry.setAdapter(adapterCountries);

        ArrayAdapter<String> adapterLanguages = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_dropdown_item,
                getResources().getStringArray(R.array.languages));
        spinnerLanguage.setAdapter(adapterLanguages);

        // zeby wartosci pol nie byly puste kiedy wracamy do trybu edycji - dotyczy tych czterech pol
        editName.setText(getSharedPreferences(PREFERENCES_PATH, MODE_PRIVATE).getString(PREFERENCE_NAME, MainActivity.DEFAULT_VALUE ));
        editSurname.setText(getSharedPreferences(PREFERENCES_PATH, MODE_PRIVATE).getString(PREFERENCE_SURNAME, MainActivity.DEFAULT_VALUE));

        spinnerLanguage.setSelection(adapterLanguages
                .getPosition(getSharedPreferences(PREFERENCES_PATH, MODE_PRIVATE)
                        .getString(PREFERENCE_LANG, MainActivity.DEFAULT_VALUE)));

        spinnerCountry.setSelection(adapterCountries
                .getPosition(getSharedPreferences(PREFERENCES_PATH, MODE_PRIVATE)
                        .getString(PREFERENCE_COUNTRY, MainActivity.DEFAULT_VALUE)));

    }

    @OnClick(R.id.button_save)
    protected void savePreferences() {

        if (editName.getText().toString().isEmpty() || editSurname.getText().toString().isEmpty()) {
            Toast.makeText(this, "Name or Surname is empty.", Toast.LENGTH_SHORT);
        }

        SharedPreferences preferences = getSharedPreferences(PREFERENCES_PATH, MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();

        editor.putString(PREFERENCE_NAME, editName.getText().toString());
        editor.putString(PREFERENCE_SURNAME, editSurname.getText().toString());
        editor.putString(PREFERENCE_COUNTRY, spinnerCountry.getSelectedItem().toString());
        editor.putString(PREFERENCE_LANG, spinnerLanguage.getSelectedItem().toString());

        if (editor.commit()) {
            Toast.makeText(this, "Preferences saved", Toast.LENGTH_SHORT).show();
        }

        finish();
    }

    @OnClick(R.id.button_cancel)
    protected void closeActivity() {
        finish();
    }


}
