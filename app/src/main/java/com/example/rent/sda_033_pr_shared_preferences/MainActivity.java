package com.example.rent.sda_033_pr_shared_preferences;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    public static final String DEFAULT_VALUE = "";

    @BindView(R.id.text_view_name)
    protected TextView textName;

    @BindView(R.id.text_view_surname)
    protected TextView textSurname;

    @BindView(R.id.text_view_country)
    protected TextView textCountry;

    @BindView(R.id.text_view_language)
    protected TextView textLanguage;

    @BindView(R.id.button_edit)
    protected Button buttonEdit;

    @OnClick(R.id.button_edit)
    protected void editPreferences(){
        Intent i = new Intent(this, EditActivity.class);
        startActivity(i);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }

    @Override
    protected void onResume() {
        super.onResume();

        SharedPreferences preferences = getSharedPreferences(EditActivity.PREFERENCES_PATH, MODE_PRIVATE);
        String nameFromPreferences = preferences.getString(EditActivity.PREFERENCE_NAME, DEFAULT_VALUE);
        String surnameFromPreferences = preferences.getString(EditActivity.PREFERENCE_SURNAME, DEFAULT_VALUE);
        String countryFromPreferences = preferences.getString(EditActivity.PREFERENCE_COUNTRY, DEFAULT_VALUE);
        String languageFromPreferences = preferences.getString(EditActivity.PREFERENCE_LANG, DEFAULT_VALUE);

        // ustawiamy w text view nasze dane z shared preferences
        textName.setText(nameFromPreferences);
        textSurname.setText(surnameFromPreferences);
        textCountry.setText(countryFromPreferences);
        textLanguage.setText(languageFromPreferences);


    }
}
